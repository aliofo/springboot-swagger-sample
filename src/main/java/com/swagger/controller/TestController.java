package com.swagger.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.swagger.domain.Menu;
import com.swagger.domain.Rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "测试菜单列表", tags = "菜单列表")
@RestController
@RequestMapping("/menu/api")
public class TestController {

	@GetMapping("/getMenuList")
	@ApiOperation(value = "获取菜单列表", tags = "菜单列表", notes = "获取菜单，考虑递归情况", httpMethod = "GET")
	public Menu getMenuList() {
		Menu menu = new Menu("caidan_system", "菜单系统");

		menu.addChildren(new Menu("label1", "菜单1"));
		menu.addChildren(new Menu("label2", "菜单2"));
		menu.addChildren(new Menu("label3", "菜单3"));
		menu.addChildren(new Menu("label4", "菜单4"));
		return menu;
	}
	
	@PostMapping("/getRestMenu")
	@ApiOperation(value = "响应泛型为菜单Menu对象",notes = "获取菜单,考虑递归的情况")
	public Rest<Menu> getRestMenu() {
		Menu menu = new Menu("caidan_system", "菜单系统");

		menu.addChildren(new Menu("label1", "菜单1"));
		menu.addChildren(new Menu("label2", "菜单2"));
		menu.addChildren(new Menu("label3", "菜单3"));
		menu.addChildren(new Menu("label4", "菜单4"));

		Rest<Menu> rest = new Rest<>();
		rest.setData(menu);
		return rest;
	}

}
